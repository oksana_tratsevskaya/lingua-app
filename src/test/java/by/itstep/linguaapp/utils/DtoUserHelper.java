package by.itstep.linguaapp.utils;

import by.itstep.linguaapp.dto.user.UserCreateDto;
import com.github.javafaker.Faker;

import static by.itstep.linguaapp.LinguaappApplicationTests.FAKER;

public class DtoUserHelper {

    public static UserCreateDto generateUserCreateDto() {
        UserCreateDto createDto = new UserCreateDto();
        createDto.setEmail(FAKER.internet().emailAddress());
        createDto.setName(FAKER.name().firstName());
        createDto.setPassword(FAKER.phoneNumber().cellPhone());
        createDto.setCountry(FAKER.country().name());
        createDto.setPassword(FAKER.internet().password());
        return createDto;
    }

}
