package by.itstep.linguaapp.mapper;

import by.itstep.linguaapp.dto.user.UserFullDto;
import by.itstep.linguaapp.entity.UserEntity;
import by.itstep.linguaapp.entity.UserRole;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UserMapperTest {

    @Autowired
    private UserMapper userMapper;

    @Test
    public void mapToDto_happyPath() {
        //given
        UserEntity userEntity = new UserEntity();
        userEntity.setPhone("375 44 7665030");
        userEntity.setName("Bob");
        userEntity.setRole(UserRole.USER);
        userEntity.setPassword("123456");
        userEntity.setCountry("DE");
        userEntity.setEmail("test@test.ru");
        userEntity.setId(15);


        //when
        UserFullDto userFullDto = userMapper.map(userEntity);

        //then
        Assertions.assertNotNull(userFullDto);
        System.out.println("---->" + userFullDto);
    }
}
