package by.itstep.linguaapp.repository;

import by.itstep.linguaapp.entity.UserEntity;
import by.itstep.linguaapp.entity.UserRole;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@SpringBootTest
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    public void create() {
        //given
        UserEntity user = new UserEntity();
        user.setCountry("Country");
        user.setEmail("email");
        user.setPassword("password");
        user.setPhone("phone");
        user.setRole(UserRole.ADMIN);
        user.setName("Name");

        //when
        userRepository.save(user);

        //then

    }

    @Test
    @Transactional
    public void update() {
        //given

        //when
        List<UserEntity> foundUser = userRepository.findAllAdmins();
        System.out.println("_________FOUND________" + foundUser);

        //then

    }

    @Test
    @Transactional
    public void delete() {
        //given

        //when
        userRepository.deleteAllAdmins();
        System.out.println("_________DELETE________");

        //then

    }

}
