package by.itstep.linguaapp.service;

import by.itstep.linguaapp.dto.question.QuestionCreateDto;
import by.itstep.linguaapp.dto.question.QuestionFullDto;
import by.itstep.linguaapp.dto.question.QuestionShortDto;
import by.itstep.linguaapp.dto.question.QuestionUpdateDto;

import java.util.List;


public interface QuestionService {

    QuestionFullDto create(QuestionCreateDto dto);

    QuestionFullDto update(QuestionUpdateDto dto);

    QuestionFullDto findById(Integer id);

    List<QuestionShortDto> findAll();

    void delete(int id);

    boolean checkAnswer(Integer id, Integer answerId);

    QuestionFullDto getRandomQuestion(Integer categoryId);

}
