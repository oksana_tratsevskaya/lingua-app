package by.itstep.linguaapp.service.impl;

import by.itstep.linguaapp.dto.answer.AnswerCreateDto;
import by.itstep.linguaapp.dto.question.QuestionCreateDto;
import by.itstep.linguaapp.dto.question.QuestionFullDto;
import by.itstep.linguaapp.dto.question.QuestionShortDto;
import by.itstep.linguaapp.dto.question.QuestionUpdateDto;
import by.itstep.linguaapp.entity.AnswerEntity;
import by.itstep.linguaapp.entity.CategoryEntity;
import by.itstep.linguaapp.entity.QuestionEntity;
import by.itstep.linguaapp.entity.UserEntity;
import by.itstep.linguaapp.exception.AppEntityNotFoundException;
import by.itstep.linguaapp.mapper.QuestionMapper;
import by.itstep.linguaapp.repository.AnswerRepository;
import by.itstep.linguaapp.repository.CategoryRepository;
import by.itstep.linguaapp.repository.QuestionRepository;
import by.itstep.linguaapp.repository.UserRepository;
import by.itstep.linguaapp.security.AuthenticationService;
import by.itstep.linguaapp.service.MailService;
import by.itstep.linguaapp.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ValidationException;


import java.time.Instant;
import java.util.List;

@Service
public class QuestionServiceImpl implements QuestionService {

    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private QuestionMapper questionMapper;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private AnswerRepository answerRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private MailService mailService;


    @Override
    @Transactional
    public QuestionFullDto create(QuestionCreateDto dto) {

        QuestionEntity questionToSave = questionMapper.map(dto);

        for (AnswerEntity answerEntity : questionToSave.getAnswers()) {
            answerEntity.setQuestion(questionToSave);
        }


        throwIfInvalidNumberOfCorrectAnswer(dto);

        List<CategoryEntity> categoryEntities = categoryRepository.findAllById(dto.getCategoryId());
        questionToSave.setCategories(categoryEntities);

        if(questionToSave == null){
            throw new AppEntityNotFoundException("QuestionServiceImpl -> Entity is empty");
        }

        QuestionEntity savedQuestion= questionRepository.save(questionToSave);

        System.out.println("QuestionServiceImpl -> Question  was created");

        return questionMapper.mapFullDto(savedQuestion);
    }


    @Override
    @Transactional
    public QuestionFullDto update(QuestionUpdateDto dto) {

        QuestionEntity entityToUpdate = questionRepository.getById(dto.getId());

        if (entityToUpdate.getId() == null) {
            throw new AppEntityNotFoundException("QuestionServiceImpl -> Was not found by id: " + dto.getId());
        }

        entityToUpdate.setDescription(dto.getDescription());
        entityToUpdate.setLevel(dto.getLevel());

        QuestionEntity updatedQuestion = questionRepository.save(entityToUpdate);

        System.out.println("QuestionServiceImpl -> Question was created");

        return questionMapper.mapFullDto(updatedQuestion);
    }

    @Override
    @Transactional
    public QuestionFullDto findById(Integer id) {

        QuestionEntity foundQuestionEntity = questionRepository.getById(id);

        if (foundQuestionEntity == null){
            throw new AppEntityNotFoundException("QuestionServiceImpl -> Was not found by id: " + id);
        }

        QuestionFullDto foundQuestionDto = questionMapper.mapFullDto(foundQuestionEntity);

        System.out.println("QuestionServiceImpl -> Question was founded");

        return foundQuestionDto;
    }

    @Override
    @Transactional
    public List<QuestionShortDto> findAll() {

        List<QuestionShortDto> questionShortDtos = questionMapper.map(questionRepository.findAll());

        System.out.println("QuestionServiceImpl -> Questions were founded");

        return questionShortDtos;
    }

    @Override
    @Transactional
    public void delete(int id) {

        QuestionEntity questionToDelete = questionRepository.getById(id);

        if(questionToDelete == null){
            throw new AppEntityNotFoundException("QuestionServiceImpl -> QuestionEntity was not found by id: " + id);
        }

        questionToDelete.setDeletedAt(Instant.now());
        for (AnswerEntity answerEntity : questionToDelete.getAnswers()) {
            answerEntity.setDeletedAt(Instant.now());
        }
        questionRepository.save(questionToDelete);

        System.out.println("QuestionServiceImpl -> Question was successfully deleted");
    }

    @Override
    @Transactional
    public boolean checkAnswer(Integer questionId, Integer answerId) {

        AnswerEntity answer = answerRepository.findByIdAndQuestionId(answerId, questionId);

        if (answer == null) {
            throw  new AppEntityNotFoundException("Answer  was not  found by id ");
        }

        if (answer.getCorrect()) {
            QuestionEntity question = answer.getQuestion();
            UserEntity user = authenticationService.getAuthenticationUser();
            question.getUserWhoCompleted().add(user);
            questionRepository.save(question);
            mailService.sendEmail(user.getEmail(), "Right answer!");
        }
        return answer.getCorrect();
    }

    @Override
    @Transactional
    public QuestionFullDto getRandomQuestion(Integer categoryId) {

        UserEntity user = authenticationService.getAuthenticationUser();

        List<QuestionEntity> foundQuestions =
                questionRepository.findNotCompleted(categoryId, user.getId());

        if(foundQuestions.isEmpty()) {
            throw new AppEntityNotFoundException("Can't find available question by category id: " + categoryId);
        }

        int randomIndex = (int)(foundQuestions.size() * Math.random());
        QuestionEntity randomQuestion = foundQuestions.get(randomIndex);
        System.out.println("QuestionServiceImpl -> ");

        return questionMapper.mapFullDto(randomQuestion);
    }

    private void throwIfInvalidNumberOfCorrectAnswer(QuestionCreateDto dto)  {
        int counter = 0;
        for (AnswerCreateDto answerCreateDto : dto.getAnswers()) {
            if(answerCreateDto.getCorrect()){
                counter++;
            }
        }
        if (counter != 1) {
            throw new ValidationException("Question must contains the only one correct answer");
        }
    }
}
