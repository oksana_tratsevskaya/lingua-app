package by.itstep.linguaapp.service.impl;

import by.itstep.linguaapp.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class MailServiceImpl implements MailService {

    @Autowired
    private JavaMailSender javaMailSender;



    @Override
    public void sendEmail(String email, String massage) {
        SimpleMailMessage mail = new SimpleMailMessage();
        mail.setSubject("Email from LinguaApp team");
        mail.setTo(email);
        mail.setText("Text from Oksana Tratsevskaya: " + massage);

        System.out.println("Send email");

        javaMailSender.send(mail);
    }
}
