package by.itstep.linguaapp.service.impl;

import by.itstep.linguaapp.dto.category.CategoryCreateDto;
import by.itstep.linguaapp.dto.category.CategoryFullDto;
import by.itstep.linguaapp.dto.category.CategoryUpdateDto;
import by.itstep.linguaapp.entity.CategoryEntity;
import by.itstep.linguaapp.exception.AppEntityNotFoundException;
import by.itstep.linguaapp.mapper.CategoryMapper;
import by.itstep.linguaapp.repository.CategoryRepository;
import by.itstep.linguaapp.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryMapper categoryMapper;

    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    @Transactional
    public CategoryFullDto create(CategoryCreateDto dto) {

        CategoryEntity entityForSave = categoryMapper.map(dto);

        CategoryEntity createdCategory = categoryRepository.save(entityForSave);

        System.out.println("CategoryServiceImpl -> Category was successfully created");

        return categoryMapper.map(createdCategory);
    }

    @Override
    @Transactional
    public CategoryFullDto update(CategoryUpdateDto dto) {

        CategoryEntity entity = categoryRepository.getById(dto.getId());

        if(entity == null) {
            throw new AppEntityNotFoundException("CategoryServiceImpl -> CategoryEntity not found with " + dto.getId());
        }

        entity.setName(dto.getName());

        CategoryEntity updatedCategory = categoryRepository.save(entity);
        System.out.println("CategoryServiceImpl -> Category was successfully updated");

        return categoryMapper.map(updatedCategory);
    }

    @Override
    @Transactional
    public CategoryFullDto findById(int id) {

        CategoryEntity entity = categoryRepository.getById(id);

        if(entity == null) {
            throw new AppEntityNotFoundException("CategoryServiceImpl -> Category was not found by id: " + id);
        }

        CategoryFullDto foundDto = categoryMapper.map(entity);
        System.out.println("CategoryServiceImpl -> Category was successfully found");

        return foundDto;
    }

    @Override
    @Transactional
    public List<CategoryFullDto> findAll() {

        List<CategoryEntity> foundCategoryEntities = categoryRepository.findAll();
        System.out.println("CategoryServiceImpl -> Categories was successfully found");

        return categoryMapper.map(foundCategoryEntities);
    }

    @Override
    @Transactional
    public void delete(int id) {

        CategoryEntity foundCategory = categoryRepository.getById(id);

        if(foundCategory == null){
            throw new AppEntityNotFoundException("CategoryServiceImpl -> CategoryEntity was not found by id: " + id);
        }

        foundCategory.setDeletedAt(Instant.now());
        categoryRepository.save(foundCategory);
        System.out.println("CategoryServiceImpl -> Category was successfully deleted");
    }
}
