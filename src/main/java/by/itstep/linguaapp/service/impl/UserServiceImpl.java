package by.itstep.linguaapp.service.impl;

import by.itstep.linguaapp.dto.user.*;
import by.itstep.linguaapp.entity.UserEntity;
import by.itstep.linguaapp.entity.UserRole;
import by.itstep.linguaapp.exception.AppEntityNotFoundException;
import by.itstep.linguaapp.exception.UniqueValueIsTakenException;
import by.itstep.linguaapp.exception.WrongUserPasswordException;
import by.itstep.linguaapp.mapper.UserMapper;
import by.itstep.linguaapp.repository.UserRepository;
import by.itstep.linguaapp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    @Transactional
    public UserFullDto create(UserCreateDto dto) {

        UserEntity userEntity = userMapper.map(dto);
        userEntity.setBlocked(false);
        userEntity.setRole(UserRole.USER);
        userEntity.setPassword(passwordEncoder.encode(dto.getPassword() + dto.getName()));

        Optional<UserEntity> entityWithSameEmail = userRepository
                .findByEmail(userEntity.getEmail());

        if(entityWithSameEmail.isPresent()) {
            throw new UniqueValueIsTakenException("UserServiceIml -> Email is taken");
        }

        UserEntity saveEntity = userRepository.save(userEntity);

        System.out.println("UserServiceIml -> User was successfully created");

        return userMapper.map(saveEntity);
    }

    @Override
    @Transactional
    public UserFullDto update(UserUpdateDto dto) {

        UserEntity userToUpdate = userRepository
                .findById(dto.getId())
                .orElseThrow(() -> new AppEntityNotFoundException(
                        "UserServiceIml -> UserEntity was not found by id: " + dto.getId())
                );

        throwIfNotCurrentUserOrAdmin(userToUpdate.getEmail());

        userToUpdate.setPhone(dto.getPhone());
        userToUpdate.setName(dto.getName());
        userToUpdate.setCountry(dto.getCountry());

        UserEntity updatedUser= userRepository.save(userToUpdate);
        UserFullDto userDto = userMapper.map(updatedUser);

        System.out.println("UserServiceIml -> User was successfully updated");
        return userDto;
    }

    @Override
    @Transactional
    public UserFullDto findById(int id) {
        UserFullDto foundUser = userRepository
                .findById(id)
                .map(user -> userMapper.map(user))
                .orElseThrow(() -> new AppEntityNotFoundException(
                        "UserServiceIml -> UserEntity was not found by id: " + id)
                );

        System.out.println("UserServiceIml -> User was successfully found");

        return foundUser;
    }

    @Override
    @Transactional
    public List<UserFullDto> findAll() {

        List<UserFullDto> usersDto= userMapper.mapList(userRepository.findAll());

        System.out.println("UserServiceIml -> Users was successfully found");

        return usersDto;
    }

    @Override
    @Transactional
    public void delete(int id) {
        UserEntity foundUser = userRepository.getById(id);

        if(foundUser == null){
            throw new AppEntityNotFoundException("UserServiceIml -> UserEntity was not found by id: " + id);
        }

        foundUser.setDeletedAt(Instant.now());
        userRepository.save(foundUser);

        System.out.println("UserServiceIml -> Users was successfully deleted");
    }

    @Override
    @Transactional
    public void changePassword(ChangeUserPasswordDto dto) {

        UserEntity userToUpdate = userRepository.getById(dto.getUserId());

        if(userToUpdate == null){
            throw new AppEntityNotFoundException("UserServiceIml -> UserEntity was not found by id: " + dto.getUserId());
        }

        throwIfNotCurrentUserOrAdmin(userToUpdate.getEmail());

        if (!userToUpdate.getPassword().equals(passwordEncoder.encode(dto.getOldPassword()+userToUpdate.getEmail()))){
            throw new WrongUserPasswordException("Wrong password");
        }

        userToUpdate.setPassword(passwordEncoder.encode(dto.getNewPassword()+userToUpdate.getEmail()));

        userRepository.save(userToUpdate);

        System.out.println("UserServiceIml -> User password was successfully updated");
    }

    @Override
    @Transactional
    public UserFullDto changeRole(ChangeUserRoleDto dto) {

        UserEntity userToUpdate = userRepository.getById(dto.getUserId());

        if(userToUpdate == null){
            throw new AppEntityNotFoundException("UserServiceIml -> UserEntity was not found by id: " + dto.getUserId());
        }

        userToUpdate.setRole(dto.getNewRole());

        UserEntity updatedUser = userRepository.save(userToUpdate);
        UserFullDto userDto = userMapper.map(updatedUser);

        System.out.println("UserServiceIml -> User role was successfully updated");

        return userDto;
    }

    @Override
    @Transactional
    public void blocked(Integer userId) {
        UserEntity entityToUpdate = userRepository.getById(userId);
        if (entityToUpdate == null) {
            throw new AppEntityNotFoundException("UserEntity was not found by id: " + userId);
        }
        entityToUpdate.setBlocked(true);
        userRepository.save(entityToUpdate);
        System.out.println("UserServiceImpl -> User was successfully updated.");
    }

    private boolean currentUserIsAdmin() {
        return SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getAuthorities()
                .stream()
                .anyMatch(grantedAuthority -> grantedAuthority.equals("ROLE_" + UserRole.ADMIN.name()));
    }

    private void throwIfNotCurrentUserOrAdmin(String email) {
        if (!currentUserIsAdmin()) {
            String currentUserEmail = SecurityContextHolder.getContext().getAuthentication().getName();
            if (!currentUserEmail.equals(email)) {
                throw new RuntimeException("Can't update! Has no permission!");
            }
        }
    }

}
