package by.itstep.linguaapp.service;

import by.itstep.linguaapp.dto.user.*;
import by.itstep.linguaapp.entity.UserEntity;

import java.util.List;

public interface UserService {

    UserFullDto create(UserCreateDto dto);

    UserFullDto update(UserUpdateDto dto);

    UserFullDto findById(int id);

    List<UserFullDto> findAll();

    void delete(int id);

    void changePassword(ChangeUserPasswordDto dto);

    UserFullDto changeRole(ChangeUserRoleDto dto);

    void blocked(Integer userId);

}
