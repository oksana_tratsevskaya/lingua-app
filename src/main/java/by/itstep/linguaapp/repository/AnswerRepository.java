package by.itstep.linguaapp.repository;

import by.itstep.linguaapp.entity.AnswerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface AnswerRepository extends JpaRepository<AnswerEntity, Integer> {

    AnswerEntity findByIdAndQuestionId(Integer answerId, Integer questionId);



}
