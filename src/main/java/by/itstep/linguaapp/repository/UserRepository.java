package by.itstep.linguaapp.repository;

import by.itstep.linguaapp.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<UserEntity, Integer> {

    Optional<UserEntity> findByEmail(String email);

    @Query(value = "SELECT * FROM users WHERE role = 'ADMIN' WHERE deleted_at IS NULL", nativeQuery = true)
    List<UserEntity> findAllAdmins();

    @Query(value = "SELECT * FROM users WHERE email LIKE %:d WHERE deleted_at IS NULL", nativeQuery = true)
    List<UserEntity> findByDomain(@Param("d") String domain);

    @Modifying
    @Query(value = "UPDATE  users SET deleted_at = now() WHERE roll = 'ADMIN'", nativeQuery = true)
    void deleteAllAdmins();

    @Query(value = "SELECT * FROM users WHERE deleted_at IS NULL", nativeQuery = true)
    List<UserEntity> findAll();

}
