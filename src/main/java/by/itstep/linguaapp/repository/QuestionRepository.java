package by.itstep.linguaapp.repository;

import by.itstep.linguaapp.entity.QuestionEntity;
import liquibase.repackaged.net.sf.jsqlparser.statement.select.Join;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface QuestionRepository extends JpaRepository<QuestionEntity, Integer> {

    @Query(nativeQuery = true,
            value = "SELECT * FROM questions q " +
                    "JOIN questions_categories qc ON q.id = qc.question_id " +
                    "JOIN categories c ON c.id = qc.category_id " +
                    "WHERE c.id = :categoryId")
    List<QuestionEntity> findAllByCategory(@Param("categoryId") Integer categoryId);


    @Query(
            nativeQuery = true,
            value = "SELECT * FROM questions q " +
                    "JOIN questions_categories qc ON q.id=qc.question_id " +
                    "WHERE q.id NOT IN (" +
                    "            SELECT question_id " +
                    "            FROM users_completed_questions " +
                    "            WHERE user_id = :userId" +
                    ") " +
                    "AND qc.category_id = :categoryId"
    )
    List<QuestionEntity> findNotCompleted(Integer categoryId, Integer userId);


    @Query(value = "SELECT * FROM questions WHERE deleted_at IS NULL", nativeQuery = true)
    List<QuestionEntity> findAll();

}
