package by.itstep.linguaapp.mapper;

import by.itstep.linguaapp.dto.user.UserCreateDto;
import by.itstep.linguaapp.dto.user.UserFullDto;
import by.itstep.linguaapp.entity.UserEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "Spring")
public interface UserMapper {

    //Если не совпадают поля @Mapping(target = "phoneNumber", source = "phone")
    UserFullDto map(UserEntity userEntity);

    UserEntity map(UserCreateDto userCreateDto);


    List<UserFullDto> mapList(List<UserEntity> entities);
//    default String[] map(String value){
//        return value.split(" ");
//    }



}
