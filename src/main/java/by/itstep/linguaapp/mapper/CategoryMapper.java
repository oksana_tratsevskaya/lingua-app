package by.itstep.linguaapp.mapper;

import by.itstep.linguaapp.dto.category.CategoryCreateDto;
import by.itstep.linguaapp.dto.category.CategoryFullDto;
import by.itstep.linguaapp.entity.CategoryEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "Spring")
public interface CategoryMapper {

    CategoryFullDto map(CategoryEntity entity);

    List<CategoryFullDto> map(List<CategoryEntity> entityList);

    CategoryEntity map(CategoryCreateDto dto);

}
