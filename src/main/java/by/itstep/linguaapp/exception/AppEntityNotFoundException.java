package by.itstep.linguaapp.exception;

public class AppEntityNotFoundException extends RuntimeException{

    public AppEntityNotFoundException(String massage){
        super(massage);
    }

}
