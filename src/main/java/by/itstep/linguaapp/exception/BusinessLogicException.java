package by.itstep.linguaapp.exception;

public class BusinessLogicException extends RuntimeException{

    public BusinessLogicException(String message) {
        super(message);
    }
}
