package by.itstep.linguaapp.exception;

public class UniqueValueIsTakenException extends RuntimeException{

    public UniqueValueIsTakenException(String message) {
        super(message);
    }

}
