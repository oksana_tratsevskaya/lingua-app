package by.itstep.linguaapp.entity;


import lombok.Data;

import javax.persistence.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table (name = "categories")
public class CategoryEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  Integer id;

    @Column(name = "name")
    private String name;

    @ManyToMany(mappedBy = "categories")
    private List<QuestionEntity> questions = new ArrayList<>();

    @Column(name =  "deleted_at")
    private Instant deletedAt;
}
