package by.itstep.linguaapp;

import liquibase.pro.packaged.S;
import org.apache.tomcat.util.security.MD5Encoder;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.util.DigestUtils;

import java.nio.charset.StandardCharsets;

@SpringBootApplication
public class LinguaappApplication {

	public static void main(String[] args) {
		SpringApplication.run(LinguaappApplication.class, args);
	}

}
