package by.itstep.linguaapp.dto.question;

import by.itstep.linguaapp.dto.answer.AnswerCreateDto;
import by.itstep.linguaapp.dto.category.CategoryFullDto;
import by.itstep.linguaapp.entity.QuestionLevel;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Data
public class QuestionCreateDto {

    @NotBlank
    private String description;

    @NotNull
    private QuestionLevel level;

    @NotEmpty
    private List<Integer> categoryId;

    @NotEmpty
    @Size(min = 2)
    private List<AnswerCreateDto> answers;

}
