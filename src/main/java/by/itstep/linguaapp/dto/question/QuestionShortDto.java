package by.itstep.linguaapp.dto.question;

import by.itstep.linguaapp.dto.category.CategoryFullDto;
import by.itstep.linguaapp.entity.QuestionLevel;
import liquibase.pro.packaged.B;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Data
public class QuestionShortDto {

    @NotNull
    private Integer id;

    @NotNull
    private String description;

    @NotEmpty
    private QuestionLevel level;

    @NotEmpty
    @Size(min = 2)
    private List<CategoryFullDto> categories;

}
