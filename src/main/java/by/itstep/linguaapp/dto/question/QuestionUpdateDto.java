package by.itstep.linguaapp.dto.question;

import by.itstep.linguaapp.entity.QuestionLevel;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class QuestionUpdateDto {

    @NotNull
    private Integer id;

    @NotNull
    private String description;

    @NotNull
    private QuestionLevel level;

}
