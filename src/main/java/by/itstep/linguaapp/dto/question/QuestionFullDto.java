package by.itstep.linguaapp.dto.question;

import by.itstep.linguaapp.dto.answer.AnswerFullDto;
import by.itstep.linguaapp.dto.category.CategoryFullDto;
import by.itstep.linguaapp.entity.QuestionLevel;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Data
public class QuestionFullDto {

    @NotNull
    private Integer id;

    @NotBlank
    private String description;

    @NotNull
    private QuestionLevel level;

    @NotEmpty
    private List<CategoryFullDto> categories;

    @NotEmpty
    @Size(min = 2)
    private List<AnswerFullDto> answers;

}
