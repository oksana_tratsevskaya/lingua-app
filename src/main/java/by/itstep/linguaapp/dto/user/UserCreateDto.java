package by.itstep.linguaapp.dto.user;

import by.itstep.linguaapp.entity.UserRole;
import lombok.Data;

import javax.validation.constraints.*;

@Data
public class UserCreateDto {

    @NotBlank
    private String name;

    @NotBlank
    @Email
    private String email;

    @NotBlank
    @Size(min = 8)
    private String password;

    @NotBlank
    @Size(min = 2, max = 2, message = "min = 2, max = 2")
    private String country;

    @Size(min = 7, max = 14)
    private String phone;

}
