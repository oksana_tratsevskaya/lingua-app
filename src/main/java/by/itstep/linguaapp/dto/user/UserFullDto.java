package by.itstep.linguaapp.dto.user;

import by.itstep.linguaapp.entity.UserRole;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class UserFullDto {

    @NotNull
    private Integer id;

    @NotBlank
    private String name;

    @NotBlank
    @Email
    private String email;

    @NotNull
    private UserRole role;

    @NotBlank
    @Size(min = 2, max = 2, message = "min = 2, max = 2")
    private String country;

    @Size(min = 7, max = 14)
    private String phone;

}
