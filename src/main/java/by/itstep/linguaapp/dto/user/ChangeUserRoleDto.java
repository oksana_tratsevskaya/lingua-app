package by.itstep.linguaapp.dto.user;

import by.itstep.linguaapp.entity.UserRole;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ChangeUserRoleDto {

    @NotNull
    private Integer userId;

    @NotNull
    private UserRole newRole;

}
