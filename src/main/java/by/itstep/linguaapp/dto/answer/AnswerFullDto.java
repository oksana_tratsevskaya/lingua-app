package by.itstep.linguaapp.dto.answer;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class AnswerFullDto {

    @NotNull
    private Integer id;

    @NotBlank
    private String body;

    @NotNull
    private Boolean correct;

}
