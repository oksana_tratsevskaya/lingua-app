package by.itstep.linguaapp.controller;

import by.itstep.linguaapp.dto.answer.AnswerFullDto;
import by.itstep.linguaapp.dto.answer.AnswerUpdateDto;
import by.itstep.linguaapp.service.AnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class AnswerController {

    @Autowired
    private AnswerService answerService;

    @PutMapping(path = "/answers")
    public AnswerFullDto update(
            @Valid
            @RequestBody AnswerUpdateDto dto
    ){
        return answerService.update(dto);
    }

    @DeleteMapping(path = "/answers/{id}")
    public void delete (@PathVariable Integer id){
        answerService.delete(id);
    }

}
