package by.itstep.linguaapp.controller;

import by.itstep.linguaapp.dto.user.*;
import by.itstep.linguaapp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController //(controller + @ResponseBody)
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping(path = "/users/{id}")
    public UserFullDto findById(@PathVariable Integer id) {
        return userService.findById(id);
    }

    @GetMapping(path = "/users")
    public List<UserFullDto> findAll() {
        return userService.findAll();
    }

    @PostMapping(path = "/users")
    public UserFullDto create(
            @Valid
            @RequestBody UserCreateDto dto
    ) {
        return userService.create(dto);
    }

    @PutMapping(path = "/users")
    public UserFullDto update(
            @Valid
            @RequestBody UserUpdateDto dto
    ) {
        return userService.update(dto);
    }

    @PutMapping(path =  "/users/changePassword")
    public void changePassword(
            @Valid
            @RequestBody ChangeUserPasswordDto dto
    ) {
        userService.changePassword(dto);
    }

    @DeleteMapping(path = "/users/{id}")
    public void deleteById (@PathVariable Integer id) {
        userService.delete(id);
    }

    @PutMapping(path = "/users/{id}/blocked")
    public void blocked(
            @Valid
            @PathVariable Integer id,
            @RequestHeader("Authorization") String authorization
    )   throws Exception {
        userService.blocked(id);
    }

    @PutMapping(path = "/users/role")
    public UserFullDto changeRole(@RequestBody ChangeUserRoleDto dto) {
        return userService.changeRole(dto);
    }


}
