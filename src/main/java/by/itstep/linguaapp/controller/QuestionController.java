package by.itstep.linguaapp.controller;

import by.itstep.linguaapp.dto.question.QuestionCreateDto;
import by.itstep.linguaapp.dto.question.QuestionFullDto;
import by.itstep.linguaapp.dto.question.QuestionShortDto;
import by.itstep.linguaapp.dto.question.QuestionUpdateDto;
import by.itstep.linguaapp.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class QuestionController {

    @Autowired
    private QuestionService questionService;

    @GetMapping(path = "/questions/{id}")
    public QuestionFullDto finById(@PathVariable Integer id) {
        return questionService.findById(id);
    }

    @GetMapping(path = "/questions")
    public List<QuestionShortDto> findAll() {
        return questionService.findAll();
    }

    @PostMapping(path = "/questions")
    public QuestionFullDto create(
            @Valid
            @RequestBody QuestionCreateDto dto
    ) {
        return questionService.create(dto);
    }

    @PutMapping(path = "/questions")
    public QuestionFullDto update(
            @Valid
            @RequestBody QuestionUpdateDto dto
    ) {
        return  questionService.update(dto);
    }

    @DeleteMapping(path = "/questions/{id}")
    public void delete(
            @Valid
            @PathVariable Integer id
    ) {
        questionService.delete(id);
    }

    @PostMapping(path = "/questions/{questionId}/answers/{answerId}/check}")
    public boolean checkAnswer(
            @Valid
            @PathVariable Integer questionId,
            @PathVariable Integer answerId
    ) {
        return questionService.checkAnswer(questionId, answerId);
    }

    //...random?userId = 17
    @GetMapping(path = "/categories/{categoryId}/questions/random")
    public QuestionFullDto getRandomQuestion(
            @PathVariable Integer categoryId
    ) {
        return questionService.getRandomQuestion(categoryId);
    }

}
